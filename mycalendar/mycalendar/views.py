import calendar
from django.http import HttpResponse
 
def index(request):
    return HttpResponse(calendar.LocaleHTMLCalendar(locale='English_England').formatyear(2019, width=4))