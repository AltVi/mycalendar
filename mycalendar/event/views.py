from django.shortcuts import render
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from .models import User, Event
from .forms import EventForm
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from passlib.hash import sha256_crypt


def index(request):
    return render(request, 'index.html')


def create(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        password = request.POST.get('password')
        conf_pass = request.POST.get('conf_pass')
        email = request.POST.get('email')
        if password == conf_pass:
            if User.objects.filter(email=email):
                raise ValidationError('This user already exist')
            else:
                user = User()
                user.name = name
                user.password = sha256_crypt.encrypt(password)
                user.email = email
                user.save()
        else:
            raise ValidationError('Passwords do not match')
    return HttpResponseRedirect("/")


def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        user = User.objects.get(email=email)
        if user:
            if sha256_crypt.verify(str(request.POST.get('password_au')), user.password):
                form = EventForm()
                data = {"events": user.event_set.all(), "user": user, "form": form}
                return render(request, 'calendar.html', context=data)
            else:
                raise ValidationError('Wrong password')
        else:
            raise ValidationError('This user not exist')
    return HttpResponseRedirect("/")


def event(request, userid):
    if request.method == 'POST':
        user = User.objects.get(id=userid)
        name = request.POST.get('name')
        day = request.POST.get('day')
        start_time = request.POST.get('start_time')
        end_time = request.POST.get('end_time')
        notes = request.POST.get('notes')
        reminder = request.POST.get('remind')
        user.event_set.create(name=name, day=day, start_time=start_time, end_time=end_time, notes=notes, reminder=reminder)
        form = EventForm()
        data = {"events": user.event_set.all(), "user": user, "form": form}
        return render(request, 'calendar.html', context=data)

def delete(request, userid, eventid):
    event = Event.objects.get(id=eventid)
    user = User.objects.get(id=userid)
    event.delete()
    form = EventForm()
    data = {"events": user.event_set.all(), "user": user, "form": form}
    return render(request, 'calendar.html', context=data)