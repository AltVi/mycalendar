from django import forms
from .models import Reminder
import datetime

class EventForm(forms.Form):
    name = forms.CharField(required=True)
    day = forms.DateField(required=True)
    start_time = forms.TimeField(required=True)
    end_time = forms.TimeField(initial=datetime.time(23, 59, 59))
    notes = forms.CharField(required=False)
    remind = forms.ChoiceField(choices=[(time.value, time.name) for time in Reminder])