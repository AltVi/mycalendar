# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from enum import Enum
from django.db import models
from django.core.exceptions import ValidationError
from django.urls import reverse

today = datetime.datetime.now()
tomorrow = today + datetime.timedelta(days=1)


class Reminder(Enum):
    hour = 1
    two_hours = 2
    four_hours = 3
    day = 4
    week = 5


class User(models.Model):
    name = models.CharField(max_length=20)
    password = models.TextField(help_text=u'Password')
    email = models.EmailField(unique=True)


class Event(models.Model):
    name = models.CharField(u'Name of the event', help_text=u'Name of the event', max_length=30)
    day = models.DateField(u'Day of the event', help_text=u'Day of the event')
    start_time = models.TimeField(u'Starting time', help_text=u'Starting time')
    end_time = models.TimeField(u'Final time', help_text=u'Final time', default=datetime.time(23, 59, 59))
    notes = models.TextField(
        u'Textual Notes', help_text=u'Textual Notes', blank=True, null=True)
    reminder = models.IntegerField(
        choices=[(time.value, time.name) for time in Reminder]
    )
    user = models.ForeignKey(User, on_delete = models.CASCADE)

    def check_overlap(self, fixed_start, fixed_end, new_start, new_end):
        overlap = False
        if new_start == fixed_end or new_end == fixed_start:
            overlap = False
        elif (new_start >= fixed_start and new_start <= fixed_end) or (new_end >= fixed_start and new_end <= fixed_end):  # innner limits
            overlap = True
        elif new_start <= fixed_start and new_end >= fixed_end:  # outter limits
            overlap = True
        return overlap

    def get_absolute_url(self):
        url = reverse('admin:%s_%s_change' % (
            self._meta.app_label, self._meta.model_name), args=[self.id])
        return u'<a href="%s">%s</a>' % (url, str(self.start_time))

    def clean(self):
        if self.end_time <= self.start_time:
            raise ValidationError('Ending times must be after starting times')

        events = Event.objects.filter(day=self.day)
        if events.exists():
            for event in events:
                if self.check_overlap(event.start_time, event.end_time, self.start_time, self.end_time):
                    raise ValidationError('There is an overlap with another event: ' + str(event.day) + ', ' + str(
                        event.start_time) + '-' + str(event.end_time))
